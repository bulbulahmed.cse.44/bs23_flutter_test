import 'package:bs23_flutter_test/app/Language/language.dart';
import 'package:bs23_flutter_test/app/controller/app_controller.dart';
import 'package:bs23_flutter_test/app/controller/share_helper.dart';
import 'package:bs23_flutter_test/app/utils/app_constant.dart';
import 'package:bs23_flutter_test/app/theme/App_themes.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';

import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'app/Widgets/list_scroll_behavior.dart';
import 'app/routes/app_pages.dart';
import 'app/Language/app_localizations.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

Language language = Language();
SharedPreferences? prefs;
double? paddingTop,appbarHeight,screenRatio,paddingBottom;
String? auth;


void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  ShareHelper.init(then: (){
    runApp(MyApp());
  });
}


class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> with AutomaticKeepAliveClientMixin{

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;

  AppController appController = Get.put(AppController(),permanent: true);


  @override
  void initState() {
    super.initState();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
  }




  @override
  Widget build(BuildContext context) {
    return GetBuilder<AppController>(
        init: appController,
        builder: (controller){
          return GetMaterialApp(
            title: AppConstant.AppName,
            debugShowCheckedModeBanner: false,
            locale: Locale(controller.appLocale.value),
            supportedLocales: LocaleHelper.getAllLocale(),
            builder: (context,child) {
              return ScrollConfiguration(
                behavior: ListScrollBehavior(),
                child: child!,
              );
            },
            localizationsDelegates: const [
              GlobalMaterialLocalizations.delegate,
              GlobalWidgetsLocalizations.delegate,
              GlobalCupertinoLocalizations.delegate,
              DefaultCupertinoLocalizations.delegate
            ],
            theme: AppThemes().lightTheme,
            darkTheme: AppThemes().darkTheme,
            initialRoute: AppPages.INITIAL,
            getPages: AppPages.routes,
          );
        }
    );
  }
}
