import 'dart:async';

import 'package:bs23_flutter_test/app/theme/app_color.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';


void SuccessMessage({required String message,Function? then}){
  Get.showSnackbar(GetSnackBar(messageText: Text(message,style: Get.textTheme.bodyText1,),backgroundColor: AppColors.green,icon: const Icon(Icons.check_circle),snackPosition: SnackPosition.TOP,duration: const Duration(seconds: 3)));

  if(then!=null) {
    Timer(const Duration(seconds: 5),(){
      if(Get.isSnackbarOpen){
        Get.back();
      }
      then();
    });
  }
}
void ErrorMessage({required var message,Function? then}){
  Get.showSnackbar(GetSnackBar(messageText: Text(message,style: Get.textTheme.bodyText1,),backgroundColor: AppColors.red,icon: const Icon(Icons.cancel),snackPosition: SnackPosition.TOP,duration: const Duration(seconds: 3),));
  if(then!=null) {
    Timer(const Duration(seconds: 5),(){
      if(Get.isSnackbarOpen){
        Get.back();
      }
      then();
    });
  }
}
void InfoMessage({required var message,Function? then}){
  Get.showSnackbar(GetSnackBar(messageText: Text(message,style: Get.textTheme.bodyText1,),backgroundColor: AppColors.yellow,icon: const Icon(Icons.info),snackPosition: SnackPosition.TOP,duration: const Duration(seconds: 3),));
  if(then!=null) {
    Timer(const Duration(seconds: 5),(){
      if(Get.isSnackbarOpen){
        Get.back();
      }
      then();
    });
  }
}