import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:intl/intl.dart';

import '../../main.dart';
enum ServerDateTime { Date, Time, Both}
class Utils{
  static Future<bool> checkConnectivity() async{
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile || connectivityResult == ConnectivityResult.wifi) {
      return true;
    }else {
      return false;
    }
  }

  static String getDateTime(String datetime,{ServerDateTime serverDateTime=ServerDateTime.Date,bool withYear=true,bool onlyDate=false}){
    try{
      if(datetime.contains('Z')){
        //2021-08-25T13:00:00.000000Z
        var dateFormat = DateFormat("MM-dd-yy HH:mm:ss"); // you can change the format here
        var utcDate = dateFormat.format(DateTime.parse(datetime).toUtc());
        var localDate = dateFormat.parse(utcDate, true).toLocal().toString();
        String createdDate = dateFormat.format(DateTime.parse(localDate));
        datetime=createdDate;
      }
    }catch(e){
      datetime=datetime.replaceAll('Z', '');
      datetime=datetime.replaceAll('T', ' ');
    }
    return datetime;
  }

}