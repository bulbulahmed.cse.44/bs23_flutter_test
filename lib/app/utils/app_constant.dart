import 'package:flutter/material.dart';

import '../Language/app_localizations.dart';
import '../Language/en.dart';


class AppConstant {
  static String AppName = 'BS23 Flutter Test';
  static String font_poppins = "Poppins";
  static String Share_Auth = 'auth';
  static String Share_Language = 'language';
  static String Error = 'error';
  static String Unauthenticated = 'Unauthenticated.';
  static String Android = 'android';
  static String iOS = 'ios';
  static String Default_Language = 'en';
  static Size defaultScreenSize = const Size(380, 720);
  static AppLocale Default_Locale = AppLocale.EN;
  static Function Default_Language_Function = EN();
  static int AnimationDelay = 375;
  static var status = "status";
  static var message = "message";
  static String? error = "error";
  static var error_description = 'error_description';

  static var item = "item";
}
