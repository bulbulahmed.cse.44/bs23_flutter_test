import 'dart:ffi';

import 'package:bs23_flutter_test/app/model/repository.dart';
import 'package:bs23_flutter_test/app/theme/app_color.dart';
import 'package:bs23_flutter_test/app/theme/dimension.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';

import '../../../../main.dart';
import '../controllers/repository_controller.dart';

class RepositoryView extends GetView<RepositoryController> {
  const RepositoryView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GetBuilder<RepositoryController>(
        init: controller,
        builder: (context) {
          return Scaffold(
            appBar: AppBar(
              title: Text(language.Repository_List),
              centerTitle: false,
              actions: [
                PopupMenuButton(
                    color: AppColors.white,
                    icon: Icon(
                      Icons.filter_list,
                      color: AppColors.white,
                    ),
                    onSelected: (value) {
                        controller.sortRepository(value.toString());
                    },
                    itemBuilder: (context) => [
                          PopupMenuItem(
                            value: "updated",
                            child: Text(
                              language.Update,
                              style: Get.textTheme.bodyText1!.copyWith(
                                color: controller.sort?.value == "updated" ? AppColors.primaryColor : AppColors.black,
                                fontWeight: controller.sort?.value == "updated" ? Dimension.textMedium:Dimension.textNormal,
                              ),
                            ),
                          ),
                          PopupMenuItem(
                            value: "star",
                            child: Text(
                              language.Star,
                              style: Get.textTheme.bodyText1!.copyWith(
                                color: controller.sort?.value == "star" ? AppColors.primaryColor : AppColors.black,
                                fontWeight: controller.sort?.value == "star" ? Dimension.textMedium:Dimension.textNormal,
                              ),
                            ),
                          ),
                        ])
              ],
            ),
            body: RefreshIndicator(
              onRefresh: controller.refreshRepository,
              child: Padding(
                padding: EdgeInsets.all(Dimension.Size_10),
                child: PagedListView<int, RepositoryItems>(
                  pagingController: controller.pagingController,
                  builderDelegate: PagedChildBuilderDelegate<RepositoryItems>(
                    itemBuilder: (context, item, index) {
                      return InkWell(
                        onTap: (){
                          controller.gotoDetails(item);
                        },
                        child: Card(
                          margin: EdgeInsets.all(Dimension.Size_5),
                          child: Padding(
                            padding: EdgeInsets.symmetric(vertical: Dimension.Size_10, horizontal: Dimension.Size_10),
                            child: Row(
                              children: [
                                Expanded(child: Text(item.name ?? "")),
                                ClipOval(
                                  child: CachedNetworkImage(
                                    height: Dimension.Size_30,
                                    width: Dimension.Size_30,
                                    imageUrl: item.owner?.avatarUrl??"",
                                    placeholder: (context, url) => CircularProgressIndicator(),
                                    errorWidget: (context, url, error) => Icon(Icons.image),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      );
                    },
                  ),
                ),
              ),
            ),
          );
        });
  }
}
