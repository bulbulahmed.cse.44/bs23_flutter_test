import 'dart:async';
import 'dart:ffi';

import 'package:bs23_flutter_test/app/Widgets/show_message.dart';
import 'package:bs23_flutter_test/app/controller/share_helper.dart';
import 'package:bs23_flutter_test/app/model/repository.dart';
import 'package:bs23_flutter_test/app/repositories/get_repository.dart';
import 'package:bs23_flutter_test/app/routes/app_pages.dart';
import 'package:bs23_flutter_test/app/utils/app_constant.dart';
import 'package:bs23_flutter_test/app/utils/url.dart';
import 'package:bs23_flutter_test/main.dart';
import 'package:get/get.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';

import '../../../utils/utils.dart';

class RepositoryController extends GetxController {
  //TODO: Implement RepositoryController
  GetRepository repository = GetRepository();
  Timer? timer;
  final PagingController<int, RepositoryItems> pagingController = PagingController(firstPageKey: 0);
  final count = 0.obs;
  String link = "${URL.Get_Repository}?q=${"flutter"}";
  Repository? repositoryList;
  RxBool isLoading = false.obs;
  RxBool isRefreshEnable = true.obs;
  RxString? sort = "".obs;
  @override
  void onInit() {
    sort?.value = ShareHelper.getRepositorySort();
    timer = Timer.periodic(const Duration(minutes: 30), (Timer t) {
      isRefreshEnable(true);
    });
    pagingController.addPageRequestListener((pageKey) {
      getRepository(pageKey);
    });
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    repository.close();
    pagingController.dispose();
    super.onClose();
  }

  Future<void> getRepository(int pageKey) async {
    isLoading(true);
    Utils.checkConnectivity().then((value) async {
      if (value) {
        try{
          var response = await repository.getRepository(
              url: "$link&page=$pageKey&sort=$sort",
              then: (data) {
                if (Repository.fromJson(data).items != null) {
                  if (repositoryList == null) {
                    repositoryList = Repository.fromJson(data);
                  } else {
                    repositoryList?.items?.addAll(Repository.fromJson(data).items!);
                  }
                  ShareHelper.storeRepository(repositoryList!);
                  ShareHelper.storeRepositoryPageNo(pageKey + 1);
                  pagingController.appendPage(Repository.fromJson(data).items!, pageKey + 1);
                } else {
                  pagingController.printInfo(info: language.No_more_data);
                }
              });
        }catch(error){
          pagingController.error = error;
        }
      } else {
        if (repositoryList == null && ShareHelper.getRepository() != null) {
          repositoryList = ShareHelper.getRepository();
          pagingController.appendPage(ShareHelper.getRepository()!.items!,ShareHelper.getRepositoryPageNo());
        }else{
          pagingController.error = "error";
        }
      }
    });
    isLoading(false);
    //update();
  }

  Future<void> refreshRepository() async {
    Utils.checkConnectivity().then((value){
      if(value){
        if (isRefreshEnable.value) {
          repositoryList = null;
          pagingController.refresh();
          isRefreshEnable(false);
          timer.reactive;
        } else {
          InfoMessage(message: language.Ref_Msg);
        }
      }else{
        InfoMessage(message: language.Check_Your_Internet_Connection);
      }
    });
  }

  Future<void> sortRepository(String? selectSort) async {
    Utils.checkConnectivity().then((value){
      if(value){
        if(selectSort != sort?.value){
          sort?.value = selectSort!;
          repositoryList = null;
          pagingController.refresh();
        }else{
          sort?.value = "";
        }
        ShareHelper.storeRepositorySort(sort?.value??"");
        update();
      }else{
        InfoMessage(message: language.Check_Your_Internet_Connection);
      }
    });
  }

  void gotoDetails(RepositoryItems item) {
    Get.toNamed(Routes.REPOSITORY_DETAILS,arguments: {
      AppConstant.item: item
    });
  }


}
