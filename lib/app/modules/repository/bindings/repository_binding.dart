import 'package:get/get.dart';

import '../controllers/repository_controller.dart';

class RepositoryBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<RepositoryController>(
      () => RepositoryController(),
    );
  }
}
