import 'package:bs23_flutter_test/app/theme/dimension.dart';
import 'package:bs23_flutter_test/main.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../../../theme/app_color.dart';
import '../../../utils/utils.dart';
import '../controllers/repository_details_controller.dart';

class RepositoryDetailsView extends GetView<RepositoryDetailsController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title:  Text(language.Repository_Details),
        centerTitle: false,
      ),
      body: SingleChildScrollView(
        child: Container(
          width: Get.width,
          padding: EdgeInsets.all(Dimension.Size_10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [

              Text(language.User_Information,style: Get.textTheme!.headline5!.copyWith(color: AppColors.primaryColor),),
              SizedBox(
                height: Dimension.Size_10,
              ),
              Card(
                child: Container(
                  width: Get.width,
                  padding: EdgeInsets.all(Dimension.Size_10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      ClipOval(
                        child: CachedNetworkImage(
                          height: Dimension.Size_150,
                          width: Dimension.Size_150,
                          imageUrl: controller.items?.owner?.avatarUrl??"",
                          placeholder: (context, url) => CircularProgressIndicator(),
                          errorWidget: (context, url, error) => Icon(Icons.image),
                        ),
                      ),
                      SizedBox(
                        height: Dimension.Size_10,
                      ),
                      Center(
                        child: Text(
                          controller.items?.owner?.login??"",
                          style: Get.textTheme.headline5!.copyWith(color: AppColors.primaryColor),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: Dimension.Size_10,
              ),
              Text(language.Repository_Information,style: Get.textTheme!.headline5!.copyWith(color: AppColors.primaryColor),),
              SizedBox(
                height: Dimension.Size_10,
              ),
              Card(
                child: Container(
                  width: Get.width,
                  padding: EdgeInsets.all(Dimension.Size_10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Center(
                        child: Text(
                          controller.items?.name??"",
                          style: Get.textTheme.headline5!.copyWith(color: AppColors.primaryColor),
                        ),
                      ),
                      SizedBox(
                        height: Dimension.Size_10,
                      ),
                      Text(
                        controller.items?.description??language.No_description,
                        style: Get.textTheme.bodyText1,
                      ),
                      SizedBox(
                        height: Dimension.Size_10,
                      ),
                      Row(
                        children: [
                          Expanded(
                            child: RichText(
                              text: TextSpan(
                                  text: "${language.Last_Update} : \n",
                                  style: Get.textTheme.bodyText1,
                                  children:[
                                    TextSpan(
                                      text: Utils.getDateTime(controller.items?.updatedAt??""),
                                      style: Get.textTheme.bodyText1,
                                    )
                                  ]
                              ),
                            ),
                          ),
                          Expanded(
                            child: RichText(
                              text: TextSpan(
                                  text: "${language.Create_At} : \n",
                                  style: Get.textTheme.bodyText1,
                                  children:[
                                    TextSpan(
                                      text: Utils.getDateTime(controller.items?.createdAt??""),
                                      style: Get.textTheme.bodyText1,
                                    )
                                  ]
                              ),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: Dimension.Size_10,
                      ),
                      RichText(
                        text: TextSpan(
                          text: "${language.Git_Url}: ",
                          style: Get.textTheme.bodyText1,
                          children:[
                            TextSpan(
                              text: controller.items?.gitUrl??"",
                              style: Get.textTheme.bodyText1!.copyWith(color: Colors.blueAccent),
                            )
                          ]
                        ),
                      ),
                      SizedBox(
                        height: Dimension.Size_10,
                      ),
                      RichText(
                        text: TextSpan(
                          text: "${language.Clone_Url}: ",
                          style: Get.textTheme.bodyText1,
                          children:[
                            TextSpan(
                              text: controller.items?.cloneUrl??"",
                              style: Get.textTheme.bodyText1!.copyWith(color: Colors.blueAccent),
                            )
                          ]
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
