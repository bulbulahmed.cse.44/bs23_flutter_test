import 'package:bs23_flutter_test/app/model/repository.dart';
import 'package:bs23_flutter_test/app/utils/app_constant.dart';
import 'package:get/get.dart';

class RepositoryDetailsController extends GetxController {
  RepositoryItems? items;
  @override
  void onInit() {
    items = Get.arguments[AppConstant.item];
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}
}
