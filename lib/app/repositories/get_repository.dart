
import '../Widgets/show_message.dart';
import '../app_client/api_client.dart';
import '../utils/app_constant.dart';

class GetRepository{
  ApiClient apiClient=ApiClient();

  void close(){
    apiClient.close();
  }

  Future getRepository({required url,required Function(dynamic) then}) async{
    await apiClient.Request(
        url: url,
        onSuccess: (Map data){
          if(data.containsKey(AppConstant.error)) {
            ErrorMessage(message: data[AppConstant.error_description]);
            then(null);
          } else {
            then(data);
          }
        },
        onError: (data){
          then(null);
        }
    );
  }

}