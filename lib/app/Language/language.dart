class Language {
  var Something_went_wrong = "Something went wrong";

  var Too_many_Attempts_please_try_after_some_time = "Too many Attempts please try after some time";

  var Check_Your_Internet_Connection = "Check your internet connection";

  String Repository_List = "Repository List";

  var Ref_Msg = "You can not refresh at this time!";

  var No_more_data = "No more data";

  String Update = "Update";

  String Star = "Star";

  String Repository_Details = "Repository Details";

  var No_description = "This repository haven't description";

  var PM = "PM";
  var AM = "AM";

  String Last_Update = "Last Update";

  var Git_Url = "Git Url";

  var Create_At = "Create At";

  var Clone_Url = "Clone Url";

  String Repository_Information = "Repository Information";
  String User_Information = "User Information";
}
