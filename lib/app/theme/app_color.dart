import 'package:flutter/material.dart';

class AppColors{
  static Color primaryColor =  const Color(0xFF3C68AF);
  static Color secondaryColor = const Color(0xFFFF8E00);
  static Color black=  Colors.black;
  static Color white=  Colors.white;
  static Color gray=  Colors.grey;
  static Color yellow=  Colors.yellow;
  static Color background=  const Color(0xFFE6E8F1);
  static Color green=  const Color(0xFF00A759);
  static Color red=  const Color(0xFFF52D6A);
  static Color purple=  const Color(0xFF711CF1);
}
