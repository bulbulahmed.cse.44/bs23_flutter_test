import 'package:bs23_flutter_test/app/theme/app_color.dart';
import 'package:bs23_flutter_test/app/theme/dimension.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';


class AppThemes {
  final lightTheme = ThemeData.light().copyWith(
    primaryColor: AppColors.primaryColor,
    cardColor: AppColors.secondaryColor,
    scaffoldBackgroundColor: AppColors.background,
    shadowColor: AppColors.background,
    backgroundColor: AppColors.background,
    appBarTheme: AppBarTheme(
      systemOverlayStyle: const SystemUiOverlayStyle(
        statusBarBrightness: Brightness.light,
      ),
      backgroundColor: AppColors.primaryColor,
      centerTitle: true,
      iconTheme: IconThemeData(
          color: AppColors.white
      ),
      elevation: 0,
      titleTextStyle: GoogleFonts.poppins(
        color: AppColors.white,
        fontSize: Dimension.Text_Size_Big,
        fontWeight: FontWeight.bold,
      ),
    ),
    cardTheme: CardTheme(
      color: AppColors.white,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
    ),
    //colorScheme: ColorScheme.fromSwatch().copyWith(secondary: AppColors.primaryColor),
    inputDecorationTheme: InputDecorationTheme(
      iconColor: AppColors.primaryColor,
      prefixIconColor: AppColors.primaryColor,
      suffixIconColor: AppColors.secondaryColor,
      focusColor: AppColors.primaryColor,
      fillColor: AppColors.background,
      filled: true,
      floatingLabelStyle:  GoogleFonts.poppins(
        color: AppColors.primaryColor, // <-- Change this
      ),
      labelStyle:  GoogleFonts.poppins(
        color: AppColors.gray, // <-- Change this
        fontSize: Dimension.Text_Size,
        fontWeight: FontWeight.w400,
        fontStyle: FontStyle.normal,
      ),
      contentPadding:
          const EdgeInsets.symmetric(horizontal: 20.0, vertical: 15.0),
      border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(50),
        borderSide: BorderSide(color: AppColors.primaryColor, width: 2.0),
      ),
      enabledBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(50),
        borderSide: BorderSide(color: AppColors.primaryColor,width: 2.0),
      ),
      focusedBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(50),
        borderSide: BorderSide(color: AppColors.primaryColor, width: 2.0),
      ),
      focusedErrorBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(50),
        borderSide: BorderSide(color: AppColors.red, width: 2.0),
      ),
      errorBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(50),
        borderSide: BorderSide(color: AppColors.red, width: 2.0),
      ),
    ),
    elevatedButtonTheme: ElevatedButtonThemeData(
      style: ElevatedButton.styleFrom(
        padding: const EdgeInsets.all(10),
        primary: AppColors.primaryColor,
        textStyle: GoogleFonts.poppins(
          color: AppColors.white,
          fontSize: Dimension.Text_Size,
          fontWeight: FontWeight.w500,
        ),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(50),
        ),
      ),
    ),
    textTheme: TextTheme(
      headline1: GoogleFonts.poppins(
        color: AppColors.black,
        fontSize: Dimension.Size_28,
        fontWeight: FontWeight.bold,
      ),
      headline2: GoogleFonts.poppins(
        color: AppColors.black,
        fontSize: Dimension.Size_26,
        fontWeight: FontWeight.bold,
      ),
      headline3: GoogleFonts.poppins(
        color: AppColors.black,
        fontSize: Dimension.Size_24,
        fontWeight: FontWeight.bold,
      ),
      headline4: GoogleFonts.poppins(
        color: AppColors.black,
        fontSize: Dimension.Size_20,
        fontWeight: FontWeight.bold,
      ),
      headline5: GoogleFonts.poppins(
        color: AppColors.black,
        fontSize: Dimension.Size_16,
        fontWeight: FontWeight.bold,
      ),
      headline6: GoogleFonts.poppins(
        color: AppColors.black,
        fontSize: Dimension.Text_Size,
        fontWeight: FontWeight.bold,
      ),
      subtitle1: GoogleFonts.poppins(
        color: AppColors.primaryColor,
        fontSize: Dimension.Text_Size_Small,
        fontWeight: FontWeight.normal,
      ),
      subtitle2: GoogleFonts.poppins(
        color: AppColors.secondaryColor,
        fontSize: Dimension.Text_Size_Small_Extra,
        fontWeight: FontWeight.normal,
      ),
      bodyText1: GoogleFonts.poppins(
        color: AppColors.black,
        fontSize: Dimension.Text_Size,
        fontWeight: FontWeight.normal,
      ),
      bodyText2: GoogleFonts.poppins(
        color: AppColors.black,
        fontSize: Dimension.Text_Size_Small,
        fontWeight: FontWeight.normal,
      ),
      button: GoogleFonts.poppins(
        color: Colors.white,
        fontSize: Dimension.Text_Size,
        fontWeight: FontWeight.w500,
      ),
    ),
  );
  final darkTheme = ThemeData.dark().copyWith(
    primaryColor: AppColors.primaryColor,
    cardColor: AppColors.secondaryColor,
    scaffoldBackgroundColor: AppColors.background,
    shadowColor: AppColors.background,
    backgroundColor: AppColors.background,
    appBarTheme: AppBarTheme(
      systemOverlayStyle: const SystemUiOverlayStyle(
        statusBarBrightness: Brightness.dark,
      ),
      centerTitle: true,
      iconTheme:  IconThemeData(
          color: AppColors.white
      ),
      backgroundColor: AppColors.secondaryColor,
      elevation: 0,
      titleTextStyle: GoogleFonts.poppins(
        color: AppColors.white,
        fontSize: Dimension.Text_Size_Big,
        fontWeight: FontWeight.bold,
      ),
    ),
    //colorScheme: ColorScheme.fromSwatch().copyWith(secondary: AppColors.primaryColor),
    inputDecorationTheme: InputDecorationTheme(
      iconColor: AppColors.primaryColor,
      prefixIconColor: AppColors.primaryColor,
      suffixIconColor: AppColors.primaryColor,
      focusColor: AppColors.primaryColor,
      fillColor: AppColors.secondaryColor,
      filled: true,
      contentPadding:
          const EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
      border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(50),
      ),
      enabledBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(50),
        borderSide: BorderSide(color: AppColors.secondaryColor),
      ),
      focusedBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(50),
        borderSide: BorderSide(color: AppColors.primaryColor, width: 2.0),
      ),
      focusedErrorBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(50),
        borderSide: BorderSide(color: AppColors.red, width: 2.0),
      ),
      errorBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(50),
        borderSide: BorderSide(color: AppColors.red, width: 2.0),
      ),
    ),
    elevatedButtonTheme: ElevatedButtonThemeData(
      style: ElevatedButton.styleFrom(
        primary: AppColors.primaryColor,
        padding: const EdgeInsets.all(10),
        textStyle: GoogleFonts.poppins(
          color: AppColors.white,
          fontSize: Dimension.Text_Size,
          fontWeight: FontWeight.w500,
        ),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(50),
        ),
      ),
    ),
    textTheme: TextTheme(
      headline1: GoogleFonts.poppins(
        color: AppColors.white,
        fontSize: Dimension.Size_28,
        fontWeight: FontWeight.bold,
      ),
      headline2: GoogleFonts.poppins(
        color: AppColors.white,
        fontSize: Dimension.Size_26,
        fontWeight: FontWeight.bold,
      ),
      headline3: GoogleFonts.poppins(
        color: AppColors.white,
        fontSize: Dimension.Size_24,
        fontWeight: FontWeight.bold,
      ),
      headline4: GoogleFonts.poppins(
        color: AppColors.white,
        fontSize: Dimension.Size_20,
        fontWeight: FontWeight.bold,
      ),
      headline5: GoogleFonts.poppins(
        color: AppColors.white,
        fontSize: Dimension.Size_16,
        fontWeight: FontWeight.bold,
      ),
      headline6: GoogleFonts.poppins(
        color: AppColors.white,
        fontSize: Dimension.Text_Size,
        fontWeight: FontWeight.bold,
      ),
      subtitle1: GoogleFonts.poppins(
        color: AppColors.primaryColor,
        fontSize: Dimension.Text_Size_Small,
        fontWeight: FontWeight.normal,
      ),
      subtitle2: GoogleFonts.poppins(
        color: AppColors.secondaryColor,
        fontSize: Dimension.Text_Size_Small_Extra,
        fontWeight: FontWeight.normal,
      ),
      bodyText1: GoogleFonts.poppins(
        color: AppColors.white,
        fontSize: Dimension.Text_Size,
        fontWeight: FontWeight.normal,
      ),
      bodyText2: GoogleFonts.poppins(
        color: AppColors.white,
        fontSize: Dimension.Text_Size_Small,
        fontWeight: FontWeight.normal,
      ),
      button: GoogleFonts.poppins(
        color: Colors.white,
        fontSize: Dimension.Text_Size,
        fontWeight: FontWeight.w500,
      ),
    ),
  );
}
