import 'package:get/get.dart';

import '../modules/repository/bindings/repository_binding.dart';
import '../modules/repository/views/repository_view.dart';
import '../modules/repository_details/bindings/repository_details_binding.dart';
import '../modules/repository_details/views/repository_details_view.dart';

part 'app_routes.dart';

class AppPages {
  AppPages._();

  static const INITIAL = Routes.REPOSITORY;

  static final routes = [
    GetPage(
      name: _Paths.REPOSITORY,
      page: () => const RepositoryView(),
      binding: RepositoryBinding(),
    ),
    GetPage(
      name: _Paths.REPOSITORY_DETAILS,
      page: () => RepositoryDetailsView(),
      binding: RepositoryDetailsBinding(),
    ),
  ];
}
