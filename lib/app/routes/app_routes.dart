part of 'app_pages.dart';
// DO NOT EDIT. This is code generated via package:get_cli/get_cli.dart

abstract class Routes {
  Routes._();
  static const REPOSITORY = _Paths.REPOSITORY;
  static const REPOSITORY_DETAILS = _Paths.REPOSITORY_DETAILS;
}

abstract class _Paths {
  _Paths._();
  static const REPOSITORY = '/repository';
  static const REPOSITORY_DETAILS = '/repository-details';
}
