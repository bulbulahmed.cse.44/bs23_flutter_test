import 'dart:convert';

import 'package:bs23_flutter_test/app/model/repository.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../main.dart';

class ShareHelper {
  ShareHelper.init({required Function then}) {
    SharedPreferences.getInstance().then((pr) {
      prefs = pr;
      then();
    });
  }

  ShareHelper();

  ShareHelper.storeRepository(Repository repository) {
    prefs!.setString('repository_item', jsonEncode(repository));
  }

  ShareHelper.storeRepositoryPageNo(int pageNo) {
    prefs!.setInt('repository_page_no', pageNo);
  }
  static int? getRepositoryPageNo() {
    return prefs!.getInt('repository_page_no');
  }
  ShareHelper.storeRepositorySort(String sort) {
    prefs!.setString('repository_sort', sort);
  }
  static String getRepositorySort() {
    return prefs!.getString('repository_sort')??"";
  }

  static Repository? getRepository() {
    return Repository.fromJson(jsonDecode(prefs!.getString("repository_item")!));
  }
}
