import 'package:bs23_flutter_test/app/theme/dimension.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../main.dart';
import '../Language/app_localizations.dart';
import '../utils/app_constant.dart';

class AppController extends GetxController {
  RxString appLocale=AppConstant.Default_Language.obs;
  Rx<AppLocale> mainLocale=AppConstant.Default_Locale.obs;
  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    screenRatio=Get.width/AppConstant.defaultScreenSize.width;
    if(screenRatio!<1){
      Dimension.scaleSize(screenRatio!,then: (){});
    }
    init();
  }
  void init()async{
    await SharedPreferences.getInstance().then((pr) {
      prefs = pr;
    });
    if(prefs!.containsKey(AppConstant.Share_Language)){
      appLocale.value=prefs!.getString(AppConstant.Share_Language)!;
      mainLocale.value=LocaleHelper.getLocale(appLocale.value);
      if(mainLocale.value!=AppLocale.EN){
        changeLocale(key: appLocale.value);
      }
    }
  }

  void changeLanguage({required String key}){
    prefs!.setString(AppConstant.Share_Language, key);
    changeLocale(key: key);
  }

  void changeLocale({required String key}) async {
    appLocale=key.obs;
    mainLocale.value=LocaleHelper.getLocale(key);
    LocaleHelper.setLanguage(mainLocale.value);
    Get.updateLocale(Locale(key));
    update();
  }
}