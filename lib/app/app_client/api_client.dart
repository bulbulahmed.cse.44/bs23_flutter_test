import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:path/path.dart';
import 'dart:developer';

import '../../main.dart';
import '../Widgets/show_message.dart';
import '../utils/app_constant.dart';

enum Method { POST, GET,PUT,DELETE,PATCH }



class ApiClient{
  List<http.Client> clients = [];
  Map<String,String> headerWithAuth() => {
    "Accept": "application/json",
    'Authorization':auth != null ? /*'${auth!.tokenType} ${auth!.accessToken}'*/ '' : ''
  };
  Map<String,String> headerWithoutAuth() => {
    "Accept": "application/json",
  };

  void close(){
    for(int i=0; i<clients.length;i++){
      clients[i].close();
    }
    clients.clear();
  }

  Future Request({required String url,Method method=Method.GET,var body,required Function onSuccess,required Function onError,bool enableShowError=true,bool withAuth=true})async{
    http.Client client = http.Client();
    clients.add(client);
    try {
      http.Response? response;
      if(method==Method.POST) {
        response = await client.post(Uri.parse(url),body: body, headers: withAuth ? headerWithAuth() : headerWithoutAuth());
      }
      else if(method==Method.DELETE) {
        response = await client.delete(Uri.parse(url), headers: withAuth ? headerWithAuth() : headerWithoutAuth());
      }
      else if(method==Method.PATCH) {
        response = await client.patch(Uri.parse(url), headers: withAuth ? headerWithAuth() : headerWithoutAuth(),body: body);
      }
      else {
        response = await client.get(Uri.parse(url), headers: withAuth ? headerWithAuth() : headerWithoutAuth());
      }
      showData(url: url,response: response.body,body: body,method: method,header: withAuth ? headerWithAuth() : headerWithoutAuth(),statusCode: response.statusCode);
      if (response.statusCode == 200 || response.statusCode == 204 || response.statusCode == 201 || response.statusCode == 401 || response.statusCode == 422 || response.statusCode == 400 || response.statusCode == 412) {
        var data;
        try{
          if(response.body.isNotEmpty) {
            data = json.decode(response.body);
          } else {
            data = {
              AppConstant.status:true
            };
          }
        } catch (e) {
          data = {
            AppConstant.status: response.statusCode>=400 ? false : true,
            AppConstant.message: response.body.toString()
          };
        }
        checkAuth(data,(){
          onSuccess(data);
        });
      } else {
        if(enableShowError) {
          ErrorMessage(message: language.Something_went_wrong);
        }
        onError({
          'error': response.statusCode == 429 ? language.Too_many_Attempts_please_try_after_some_time : language.Something_went_wrong
        });
      }
    } on http.ClientException catch (e) {
      debugPrint("Client Closed = ${e.toString()}");
    } on TimeoutException catch (e) {
      debugPrint(e.toString());
      if(enableShowError) {
        ErrorMessage(message: language.Check_Your_Internet_Connection);
      }
      onError({'error': language.Check_Your_Internet_Connection});
    } on SocketException catch (e) {
      debugPrint(e.toString());
      if(enableShowError) {
        ErrorMessage(message: language.Check_Your_Internet_Connection);
      }
      onError({'error': language.Check_Your_Internet_Connection});
    } on Error catch (e) {
      if(enableShowError) {
        ErrorMessage(message: language.Something_went_wrong);
      }
      onError({'error': language.Something_went_wrong});
    }
  }

  Future RequestWithFile({required String url,Map<String,String>? body,List<String>? fileKey,List<File>? files,Method method=Method.POST,required Function onSuccess,required Function onError,bool enableShowError=true,bool withAuth= true})async{
    var result;
    var uri = Uri.parse(url);
    http.MultipartRequest? request;

    request = http.MultipartRequest(getMethodName(method), uri)..fields.addAll(body!)..headers.addAll(withAuth ? headerWithAuth() : headerWithoutAuth());

    for(int i=0;i<fileKey!.length;i++){
      var stream = http.ByteStream(files![i].openRead().cast());
      var length = await files[i].length();
      var multipartFile = http.MultipartFile(fileKey[i], stream, length, filename: basename(files[i].path));
      request.files.add(multipartFile);
    }
    http.StreamedResponse? response;
    try {
      response = await request.send();
      if (response.statusCode == 200 || response.statusCode == 204 || response.statusCode == 201 || response.statusCode == 401 || response.statusCode == 422 || response.statusCode == 400 || response.statusCode == 412) {
        await response.stream.transform(utf8.decoder).listen((value) {
          result = value;
        });
        showData(body: body,method: method,response: result,url: url,header: withAuth ? headerWithAuth() : headerWithoutAuth(),statusCode: response.statusCode);
        var data=json.decode(result);
        checkAuth(data,(){
          onSuccess(data);
        });
      } else {
        if(enableShowError) {
          ErrorMessage(message: language.Something_went_wrong);
        }
        onError({'error': language.Something_went_wrong});
      }
    }on TimeoutException catch (e) {
      if(enableShowError) {
        ErrorMessage(message: language.Check_Your_Internet_Connection);
      }
      onError({'error': language.Check_Your_Internet_Connection});
    } on SocketException catch (e) {
      if(enableShowError) {
        ErrorMessage(message: language.Check_Your_Internet_Connection);
      }
      onError({'error': language.Check_Your_Internet_Connection});
    } on Error catch (e) {
      if(enableShowError) {
        ErrorMessage(message: language.Check_Your_Internet_Connection);
      }
      onError({'error': language.Check_Your_Internet_Connection});
    }
  }

  String getMethodName(Method method){
    switch(method) {
      case Method.PATCH:
        return 'PATCH';
      case Method.POST:
        return 'POST';
      default:
        return 'POST';
    }
  }

  void checkAuth(Map response,VoidCallback then){
    if(response.containsKey(AppConstant.message) && response[AppConstant.message]=='Unauthenticated.'){
      close();
      // if(auth!=null) {
      //   ErrorMessage(message: response[AppConstant.message]);
      // }
    } else {
      then();
    }
  }




  void showData({required String url, var body,required Map<String, dynamic> header,required int statusCode, required String response,required Method method}) {
    if (kDebugMode) {
      log("URL = $url");
      log("Body = $body");
      log("Status Code = $statusCode");
      log("Method = $method");
      log("Header = $header");
      log("Response = $response");
    }
  }

}



