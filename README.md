## bs23_flutter_test

here i am using getx state management, MVVM architecture.

<a href="https://drive.google.com/file/d/1lJhNO-dnzf0aHJNPeHKrgTYs3S0bU251/view?usp=sharing">
<img height="60" src="https://tecmanic.com/envato/android-app.png">
</a>

# Screenshots

Repository List page             |  Repository sort            |  Repository Details
:-------------------------:|:-------------------------:|:-------------------------:
![ScreenShot4](/asset/image.png?raw=true)  |  ![ScreenShot5](/asset/image2.png?raw=true) |  ![ScreenShot6](/asset/image3.png?raw=true)

